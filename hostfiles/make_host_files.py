import os
import time
import datetime

def sh(cmd):
  return os.system(cmd)

def generate_host_file(nodes, vcpus_per_node):
	result = ''
	for i in range(0,nodes):
		result += f'worker{i} slots={vcpus_per_node}\n'
	return result
	
sh('mkdir hostfiles')	

processing_nodes = [1,2,3,4]
core_sizes = [1,2,4,8,16]

for p in processing_nodes:
	for n in core_sizes:
		file_name = f'hostfile_{p}_{n}'
		sh(f'touch hostfiles/{file_name}')
		sh(f'cat <<EOF > {file_name}\n' + 
			generate_host_file(p,n) + 'EOF\n')





