import os
import time
import datetime

def sh(cmd):
  return os.system(cmd)


execution_times = []
nodes_nums  = []


f = open("results.txt", "a")
f.write('\n>>Starting new tests<<\n')
f.write('title;loss;accuracy;total_time;training_time;epochs;batch_size;learning_rate\n')
f.close()


processing_nodes = [1, 2, 3, 4, 5]
# core_sizes = [1,2,4,8]
learning_rates = [0.001, 0.004, 0.008]
batch_sizes = [8, 16]


iterations = 4
target_loss_value = 0


for i in range(iterations):
  for p in processing_nodes:
    for lr in learning_rates:
      for b in batch_sizes:
        cmd  = f'horovodrun -np {p} '
        cmd += f'-hostfile ../../hostfiles/hostfile_{p}_1 '
        
        cmd += f'python resnet18_solution.py '
        cmd += f'--result-file-title res_{p} '
        cmd += f'--batch-size {b} '                
        cmd += f'--learning-rate {lr} '
        cmd += f'--loss-value {target_loss_value} '

        print('command:', cmd)
        sh(cmd)




