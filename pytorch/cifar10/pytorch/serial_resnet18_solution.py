import torch
import torchvision
import torchvision.transforms as transforms
import torchvision.models as models
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import time
import argparse
import datetime

#import horovod.torch as hvd

start_time = time.time()
#hvd.init()
#########################parameters############################################
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=16, metavar='N',
                    help='input batch size for training (default: 128)')
parser.add_argument('--epochs', type=int, default=6, metavar='N',
                    help='number of epochs to train (default: 24)')
parser.add_argument('--learning-rate', type=float, default=0.001, metavar='N',
                    help='learning rate (default: 1.0)')
parser.add_argument('--result-file-title', type=str, default='default', metavar='N',
                    help='a name for this experiment')
parser.add_argument('--loss-value', type=float, default=0.1, metavar='N',
                    help='loss required to stop training (default: 0.2)')

args = parser.parse_args()



### load dataset
num_classes = 10

transform = transforms.Compose(
    [transforms.Resize(256),
     transforms.CenterCrop(224),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='./dataset', train=True,
                                        download=True, transform=transform)
trainsampler = torch.utils.data.distributed.DistributedSampler(trainset,
                                        num_replicas=1, rank=0)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size,
                                          shuffle=False, num_workers=2, sampler=trainsampler)

testset = torchvision.datasets.CIFAR10(root='./dataset', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size,
                                         shuffle=False, num_workers=2)


### create model
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.resnet = models.resnet18(pretrained=False)
        self.fc = nn.Linear(1000, num_classes)

    def forward(self, x):
        x = self.resnet(x)
        x = self.fc(x)
        return x

net = Net()
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=args.learning_rate, momentum=0.9)
# Add Horovod Distributed Optimizer
#optimizer = hvd.DistributedOptimizer(optimizer, named_parameters=net.named_parameters())


# begin training 
# loop over the dataset multiple times
n = len(trainloader)
instances_number = 0
training_time = time.time()
# Broadcast parameters from rank 0 to all other processes.
#hvd.broadcast_parameters(net.state_dict(), root_rank=0)

for epoch in range(args.epochs):  
    running_loss = 0.0
    for i, data in enumerate(trainloader):
        if(i%5 == 0):
          print("progress {}%".format(100*i/len(trainloader)))
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        #print(i, data)
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        instances_number += args.batch_size
        if(instances_number >= 4096):
          break
#         print statistics
#        running_loss += loss.item()
#        if hvd.rank() >= 0 and i % 10 == 0:    # print every 10 mini-batches
#            print('Progress', i/n)
#            running_loss = 0.0
#    if running_loss <= args.loss_value:
#        break

training_time = time.time() - training_time
# finished training

# predict test dataset
if 0 == 0:
    print('Predicting...')
    correct = 0
    total = 0
    score = 0
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            break
    test_accuracy = correct/total
    print(f"Precision ({correct}/{total}) = {test_accuracy}")
    
    elapsed_time = time.time() - start_time
    
    f = open("results.txt", "a")
    f.write(
      args.result_file_title + ";" +
      str(score) + ';' +
      str(test_accuracy) + ';' + 
      str(datetime.timedelta(seconds=elapsed_time)) + ';' +
      str(datetime.timedelta(seconds=training_time)) + ';' +
      str(instances_number) + ";" + 
      str(args.epochs) + ';' +
      str(args.batch_size) + ';' +
      str(args.learning_rate) + '\n'
      )
    f.close()
