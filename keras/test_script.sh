
echo "title;model;dataset;loss;accuracy;total_time;training_time;instances_processed;batch_size;learning_rate" >> results.txt
processing_nodes="2 1"
learning_rates="0.004"
batch_sizes="128 256 512"
workloads="resnet50,cifar10 mobilenet,mnist"
iterations=5
target_loss_value=-1
epochs=8

## aws enviromment ##
function runclust(){ while read -u 10 host; do host=${host%% slots*}; if [ ""$3"" == "verbose" ]; then echo "On $host"; fi; ssh -o "StrictHostKeyChecking no" $host ""$2""; done 10<$1; };
runclust hosts "echo 'Activating tensorflow_p37'; tmux new-session -s activation_tf -d \"source activate tensorflow_p37 > activation_log.txt;\"" verbose;
runclust hosts "while tmux has-session -t activation_tf 2>/dev/null; do :; done; cat activation_log.txt"
source activate tensorflow_p37
echo "Launching training job with synthetic data using $gpus CPUs?"
set -ex
INSTANCE_TYPE=`curl http://169.254.169.254/latest/meta-data/instance-type 2>>/var/tmp/${CONDA_DEFAULT_ENV}.err`
if [  -n "$(uname -a | grep Ubuntu)" ]; then INTERFACE=ens3; if [ $INSTANCE_TYPE == "p3dn.24xlarge" ]; then INTERFACE=ens5; fi ; else INTERFACE=eth0; fi
## end aws enviromment ##

for workload in $workloads; do
  for num_p in $processing_nodes; do
    for learning_rate in $learning_rates; do
      for batch_size in $batch_sizes; do
        model=$(echo "$workload" | cut -d ","  -f 1)
        dataset=$(echo "$workload" | cut -d ","  -f 2)
        host_file="../hostfiles/hostfile_""$num_p""_1"
        title="cluster_""$INSTANCE_TYPE""_size_$num_p"

        ./memory_script.sh "RUNNING: $model;$dataset;$num_p;$learning_rate;$batch_size" >> memory_log.txt &

        echo "RUNNING: $model;$dataset;$num_p;$learning_rate;$batch_size"
        /opt/amazon/openmpi/bin/mpirun -np $num_p -hostfile $host_file -mca plm_rsh_no_tree_spawn 1 \
          -bind-to socket -map-by slot \
          -x HOROVOD_HIERARCHICAL_ALLREDUCE=1 -x HOROVOD_FUSION_THRESHOLD=16777216 \
          -x NCCL_MIN_NRINGS=4 -x LD_LIBRARY_PATH -x PATH -mca pml ob1 -mca btl ^openib \
          -x NCCL_SOCKET_IFNAME=$INTERFACE -mca btl_tcp_if_exclude lo,docker0 \
          -x TF_CPP_MIN_LOG_LEVEL=0 \
        python3 solver.py \
          --result-file-title $title \
          --batch-size $batch_size \
          --learning-rate 0.004 \
          --loss-value -1 \
          --model $model \
          --data-set $dataset \
          --epochs 4 \
          --data-base-size 2048
        sleep 10
      done
    done
  done
done


