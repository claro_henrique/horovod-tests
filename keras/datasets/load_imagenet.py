############Load libraries#####################################################
import pickle
###############################################################################


data_dir = './datasets/imagenet_mock_dataset.pkl'

def load_imagenet_data():
    with open(data_dir, 'rb') as f:
        df = pickle.load(f, encoding='bytes')
    return df

