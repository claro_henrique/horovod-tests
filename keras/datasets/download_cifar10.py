import pickle
import numpy as np
import tensorflow as tf

cf10 = tf.keras.datasets.cifar10.load_data()

with open('cifar10_dataset.pkl', 'wb') as f:
    pickle.dump(cf10, f, pickle.HIGHEST_PROTOCOL)
    