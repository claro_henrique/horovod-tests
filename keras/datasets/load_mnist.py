############Load libraries#####################################################
import pickle
###############################################################################


data_dir = './datasets/mnist_dataset.pkl'

def load_mnist_data():
    with open(data_dir, 'rb') as f:
        df = pickle.load(f, encoding='bytes')
    return df

