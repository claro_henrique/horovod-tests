############Load libraries#####################################################
import pickle
###############################################################################


data_dir = './datasets/cifar10_dataset.pkl'


def load_cifar10_data():
    with open(data_dir, 'rb') as f:
        df = pickle.load(f, encoding='bytes')
    return df

