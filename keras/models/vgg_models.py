

from tensorflow.keras.applications import VGG16
from tensorflow.keras.applications import VGG19


def vgg16(img_rows, img_cols, channels, num_classes):
  input_shape=(img_rows,img_cols,channels) if channels else (img_rows,img_cols)
  return VGG16(include_top=True,
                  weights=None,
                  input_shape=input_shape,
                  classes=num_classes)

def vgg19(img_rows, img_cols, channels, num_classes):
  input_shape=(img_rows,img_cols,channels) if channels else (img_rows,img_cols)
  return VGG19(include_top=True,
                  weights=None,
                  input_shape=input_shape,
                  classes=num_classes)
