from tensorflow.keras.applications import ResNet50
from tensorflow.keras.applications import ResNet101


def resnet50(img_rows, img_cols, channels, num_classes):
  input_shape=(img_rows,img_cols,channels) if channels else (img_rows,img_cols)
  return ResNet50(include_top=True,
                  weights=None,
                  input_shape=input_shape,
                  classes=num_classes)

def resnet101(img_rows, img_cols, channels, num_classes):
  input_shape=(img_rows,img_cols,channels) if channels else (img_rows,img_cols)
  return ResNet101(include_top=True,
                  weights=None,
                  input_shape=input_shape,
                  classes=num_classes)







