from tensorflow.keras.applications import MobileNet
from tensorflow.keras.applications import MobileNetV2


def mobilenet(img_rows, img_cols, channels, num_classes):
  input_shape=(img_rows,img_cols,channels) if channels else (img_rows,img_cols)
  return MobileNet(include_top=True,
                  weights=None,
                  input_shape=input_shape,
                  classes=num_classes)

def mobilenetv2(img_rows, img_cols, channels, num_classes):
  input_shape=(img_rows,img_cols,channels) if channels else (img_rows,img_cols)
  return MobileNetV2(include_top=True,
                  weights=None,
                  input_shape=input_shape,
                  classes=num_classes)







