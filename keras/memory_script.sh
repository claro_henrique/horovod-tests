comment=$1

echo ""
echo "starting memory count: $comment"

runtime="2 minute"
endtime=$(date -ud "$runtime" +%s)

while [[ $(date -u +%s) -le $endtime ]]
do
  timestamp="$(date +%s%N)"
  memory_usage="$(free | grep 'Mem:' | awk '{print $3}')"
  memory_log="$timestamp $memory_usage"
  sleep 0.4
  echo $memory_log
done

echo ""
