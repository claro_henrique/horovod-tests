###############################################################################
#this code helps to train a sequential custom model on the dataset of your interest and 
#visualize the confusion matrix, ROC and AUC curves
###############################################################################
#load the libraries
import numpy as np
import time
import argparse
import datetime
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Conv2D, Activation, Dense, MaxPooling2D, Flatten, Dropout
from tensorflow.keras.callbacks import Callback
from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss
from tensorflow.keras.optimizers import SGD
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import classification_report,confusion_matrix, accuracy_score
from sklearn.metrics import average_precision_score
from keras.utils import np_utils

#load dataset
from datasets.load_mnist import load_mnist_data
from datasets.load_cifar10 import load_cifar10_data
from datasets.load_cifar100 import load_cifar100_data
from datasets.load_imagenet import load_imagenet_data

#load models
from models.custom_models import cnn_plain_model
from models.resnet_models import resnet50
from models.resnet_models import resnet101
from models.vgg_models import vgg16
from models.vgg_models import vgg19
from models.mobilenet_models import mobilenet
from models.mobilenet_models import mobilenetv2

#load the horovod
import horovod.tensorflow.keras as hvd

start_time = time.time()
#########################parameters############################################
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=128, metavar='N',
                    help='input batch size for training (default: 128)')
parser.add_argument('--epochs', type=int, default=24, metavar='N',
                    help='number of epochs to train (default: 24)')
parser.add_argument('--learning-rate', type=float, default=0.00001, metavar='N',
                    help='learning rate (default: 1.0)')
parser.add_argument('--internal-np', type=int, default=1, metavar='N',
                    help='number threads to use in tensorflow')
parser.add_argument('--result-file-title', type=str, default='default', metavar='N',
                    help='a name for this experiment')
parser.add_argument('--loss-value', type=float, default=0.1, metavar='F',
                    help='loss required to stop training (default: 0.2)')
parser.add_argument('--data-base-size', type=int, default=999999999, metavar='N',
                    help='database max limit size (default: 99999999')
parser.add_argument('--model', type=str, default='resnet50', metavar='S',
                    help='deep learning model to be used (default: resnet50')
parser.add_argument('--data-set', type=str, default='cifar10', metavar='S',
                    help='dataset to be used (default: cifar10')
parser.add_argument("--run-serial", default=False, action="store_true")

args = parser.parse_args()

run_parallel = not args.run_serial
if run_parallel:
  hvd.init()

#########################image characteristics#################################


if args.data_set == 'mnist':
  img_rows = 32
  img_cols = 32
  channel = 1
  num_classes = 10
  (x_train, y_train), (x_test, y_test) = load_mnist_data()

if args.data_set == 'cifar10':
  img_rows = 32
  img_cols = 32
  channel = 3
  num_classes = 10
  (x_train, y_train), (x_test, y_test) = load_cifar10_data()

if args.data_set == 'cifar100':
  img_rows = 32
  img_cols = 32
  channel = 3
  num_classes = 100
  (x_train, y_train), (x_test, y_test) = load_cifar100_data()

if args.data_set == 'imagenet':
  img_rows = 224
  img_cols = 224
  channel = 3
  num_classes = 1000
  (x_train, y_train), (x_test, y_test) = load_imagenet_data()

x_train = x_train[:args.data_base_size]
y_train = y_train[:args.data_base_size]
x_test = x_test[:args.data_base_size]
y_test = y_test[:args.data_base_size]

#normalize data
x_train = x_train / 255
x_test = x_test / 255

#adjust shape
if(channel == None):
  x_train = np.expand_dims(x_train, -1)
  x_test = np.expand_dims(x_test, -1)
  channel = 1

# transform label shape
y_train = np_utils.to_categorical(y_train, num_classes)
y_test = np_utils.to_categorical(y_test, num_classes)

#print the shape of the data
print('train shape:', x_train.shape, y_train.shape)
print('test shape:', x_test.shape, y_test.shape)


######################### model characteristics #################################
batch_size = args.batch_size
num_epoch = args.epochs
train_batches = len(x_train) // batch_size
test_batches = len(x_test) // batch_size


###############################################################################
# configuring the customized model and optimizer #
if args.model == 'resnet50':
  model = resnet50(img_rows, img_cols, channel, num_classes)
if args.model == 'resnet101':
  model = resnet101(img_rows, img_cols, channel, num_classes)
if args.model == 'vgg16':
  model = vgg16(img_rows, img_cols, channel, num_classes)
if args.model == 'vgg19':
  model = vgg19(img_rows, img_cols, channel, num_classes)
if args.model == 'mobilenet':
  model = mobilenet(img_rows, img_cols, channel, num_classes)
if args.model == 'mobilenetv2':
  model = mobilenet(img_rows, img_cols, channel, num_classes)


opt = SGD(lr=args.learning_rate, momentum=0.9)
if run_parallel:
  opt = hvd.DistributedOptimizer(opt)

# compile model
model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

#create stop at loss value callback
epoch_counter = 0
class stopAtLossValue(Callback):
  def on_epoch_end(self, epoch, logs={}):
    global epoch_counter
    epoch_counter += 1
    THR = args.loss_value # Assign THR with the value at which you want to stop training.
    if logs.get('loss') <= THR:
         self.model.stop_training = True


#define callbacks
if run_parallel:
  callbacks = [
      # Horovod: broadcast initial variable states from rank 0 to all other processes.
      # This is necessary to ensure consistent initialization of all workers when
      # training is started with random weights or restored from a checkpoint.    
      hvd.callbacks.BroadcastGlobalVariablesCallback(0),

      # Horovod: average metrics among workers at the end of every epoch.
      #
      # Note: This callback must be in the list before the ReduceLROnPlateau,
      # TensorBoard or other metrics-based callbacks.
      #hvd.callbacks.MetricAverageCallback(),
      # Reduce the learning rate if training plateaues.
      #keras.callbacks.ReduceLROnPlateau(patience=10, verbose=1),
      # Stop when the value Loss is less than THR
      stopAtLossValue(),]
else:
  callbacks = [
      # Stop when the value Loss is less than THR
      stopAtLossValue(),]


###############################################################################
#load data


#wait all nodes' data loading^M
print('Data loading barrier')
#hvd.allreduce([0])
#hvd.allreduce([], name="Barrier")
print('End of data loading barrier')

###############################################################################
#train the model
if run_parallel:
  steps_per_epoch = train_batches #//hvd.size()
else:
  steps_per_epoch = train_batches

training_time = time.time()
hist = model.fit( x_train, y_train,
                  batch_size=batch_size,
                  callbacks=callbacks,
                  epochs=num_epoch,
                  verbose=1)

#compute the training time
training_time = time.time() - training_time
print('-'*30)
print('Training time:', training_time)

###############################################################################
#predict on the validation data
y_pred = model.predict(x_test, batch_size=batch_size, verbose=1)

# compute the accuracy
test_accuracy = accuracy_score(y_test.argmax(axis=-1),y_pred.argmax(axis=-1))
print('Test_Accuracy:', test_accuracy)

# compute the cross-entropy loss score
score = log_loss(y_test,y_pred)
print('Score:', score)


if not run_parallel or hvd.rank() == 0:
	elapsed_time = time.time() - start_time
	num_instances = batch_size * steps_per_epoch * args.epochs
	if run_parallel:
		num_instances *= hvd.size()
	f = open("results.txt", "a")
	f.write(
		args.result_file_title + ";" +
		args.model + ';' +
                args.data_set + ';' +
                str(score) + ';' +
		str(test_accuracy) + ';' +
		str(datetime.timedelta(seconds=elapsed_time)) + ';' +
		str(datetime.timedelta(seconds=training_time)) + ';' +
		str(num_instances) + ';' +
		str(args.batch_size) + ';' +
		str(args.learning_rate) + '\n'
		)
	f.close()


#################compute confusion matrix######################################
###############################################################################
# visualizing losses and accuracy
#train_loss=hist.history['loss']

###############################################################################
